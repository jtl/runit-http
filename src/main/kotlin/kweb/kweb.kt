
package kweb

import kommon.readFileData
import kommon.runCommand
import kommon.writeToFileData
import kotlinx.cinterop.*
import platform.posix.*
import org.konan.libcurl.CUrl

fun error(msg: String) {
    println(msg)
    exit(1)
}

val OK_RESPONSE = "200 OK".cstr

val SERVICE_PATH = "/opt/service"

fun web(fd: Int, hit: Int) {

}

data class HttpRequest(val method: String, val path: String, val headers: Map<String, String>, val fd: Int)
data class HttpResponse(val body: String, val statusCode: Int = 200, val statusMessage: String = "OK")

val NOT_FOUND = HttpResponse("Not Found.", 404, "NOT FOUND")
val FORKED = HttpResponse("", 0)

typealias HttpHandler = (HttpRequest) -> HttpResponse

fun serviceStat(name: String) =
    readFileData("$SERVICE_PATH/$name/supervise/stat")?.stringFromUtf8() ?: "error"

fun servicePort(name: String): Int =
        readFileData("$SERVICE_PATH/$name/port")?.stringFromUtf8()?.toInt() ?: 0


fun handleForked(req: HttpRequest, h: HttpHandler): HttpResponse {
    val pid = fork()
    if (pid != 0) {
        return FORKED
    }
    return h(req)
}

fun serviceCommand(name: String, cmd: Char) {
    val file = "$SERVICE_PATH/$name/supervise/control"

    if (access(file, F_OK) != -1) /* file exists */
        writeToFileData(file, byteArrayOf(cmd.toByte()))
}

fun getStatus(req: HttpRequest): HttpResponse {
    val serviceName = req.headers["host"] ?: return NOT_FOUND
    return HttpResponse(serviceStat(serviceName))
}

fun setStatus(req: HttpRequest): HttpResponse {
    val serviceName = req.headers["host"] ?: return NOT_FOUND

    serviceCommand(serviceName, when (req.path) {
        "/up" -> 'u'
        "/down" -> 'd'
        else -> return NOT_FOUND
    })

    return HttpResponse(serviceStat(serviceName))

}

fun isRunning(name: String) = serviceStat(name).startsWith("run")

fun isUp(host: String, port: Int): Boolean {
    val curl = CUrl("http://$host:$port/ping")
    var success: Boolean? = null
    curl.header += {
        if (success == null) {
            val line = it.firstLine
            println("response line: $line")
            success = line.contains("200")
        }
    }
    curl.timeout(1)
    try {
        curl.fetch()
    } catch(e: CUrl.CurlException) {
        println(e)
    }
    curl.close()
    return success ?: false
}

fun ping(req: HttpRequest): HttpResponse {
    val serviceName = req.headers["host"] ?: throw RuntimeException("Missing host header")
    val port = servicePort(serviceName).also {
        if (it < 1) return NOT_FOUND }

    val start = now()
    val up = isUp("127.0.0.1", port)
    val duration = now() - start

    return if (up)
        HttpResponse("ping success. duration=$duration\n")
    else
        HttpResponse("ping failed! duration=$duration\n", 500, "PING FAIL")
}

fun handlerServiceCheck(req: HttpRequest): HttpResponse {
    val serviceName = req.headers["host"] ?: throw RuntimeException("Missing host header")

    if(!isRunning(serviceName)) {
        serviceCommand(serviceName, 'u')
    }

    val port = servicePort(serviceName).also {
        if (it < 1) return HttpResponse("Missing Port.", 404, "NOT FOUND") }

    var attempts = 5
    var up = false

    while(!up && attempts-- > 0) {
        println("ping... [$port]")
        up = isUp("127.0.0.1", port)
        sleep(1)
    }

    return if (up) {
        HttpResponse("UP")
    } else {
        HttpResponse("DOWN", 507, "PING EXCEEDED")
    }
}

fun main(args: Array<String>) {
    startHttp { req ->
        when (req.method.toLowerCase()) {
            "get" -> when(req.path) {
                "/up" -> setStatus(req)
                "/down" -> setStatus(req)
                "/status" -> getStatus(req)
                "/ping" -> ping(req)
                "/check" -> handlerServiceCheck(req)
                "/google" -> { isUp("google.com", 80).let { HttpResponse(if (it) "UP" else "DOWN") } }
                "/amzn" -> { isUp("amazon.com", 80).let { HttpResponse(if (it) "UP" else "DOWN") } }
                "/timeout" -> { isUp("test.com", 81).let { HttpResponse(if (it) "UP" else "DOWN") } }
                else -> NOT_FOUND
            }
            else -> NOT_FOUND
        }
    }
}

fun startHttp(port: Short = 8081, shouldFork: Boolean = false, handler: HttpHandler) {
    memScoped {
        var serverAddr: sockaddr_in = alloc() /* static = initialised to zeros */
        val bufferLength = 2048L
        val buffer = allocArray<ByteVar>(bufferLength)

        //val port: Short = 8081
        with(serverAddr) {
            memset(this.ptr, 0, sockaddr_in.size)
            sin_family = AF_INET.narrow()
            sin_addr.s_addr = INADDR_ANY
            sin_port = htons(port)
        }

        val listenfd = socket(AF_INET, SOCK_STREAM, 0)
        if (listenfd < 0)
            error("socket system call fail")

        if (bind(listenfd, serverAddr.ptr.reinterpret(), sockaddr_in.size.toInt()) < 0)
            error("Bind failed :(")

        if (listen(listenfd, 128) < 0)
            error("listen failed :(")

        var hit = 0
        while (true) {
            val socketfd = accept(listenfd, null, null)
            if (socketfd < 0)
                error("failure accepting socket :(")

            val childpid = if (shouldFork) fork() else 0 // fork()
            if (childpid < 0) {
                println("fork error :(")
            } else {
                if (childpid == 0) {
                    if (shouldFork) close(listenfd)
                    read(socketfd, buffer, bufferLength).toInt().let {
                        hit++

                        val end = when {
                            //it > 0 && it < bufferLength -> it
                            it > 0 -> it
                            else -> 0
                        }

                        buffer[end] = 0

                        val lines = buffer.toKString().split('\n')
                        val lastIndexHeader = lines.indexOf("").let { if(it == -1) lines.size else it }

                        /*
                        println("read, count=$it end=$end lines=${lines.size} " +
                                "lastIndexHeader=$lastIndexHeader hit=$hit")
                                */

                        if (lines.size >= 1) {
                            val request = lines.first().split(' ')
                            val method = request[0]
                            val path = request[1]
                            val requestHeaders = lines.subList(1, lastIndexHeader - 1)
                                    .map { it.replace("\r", "").replace("\n", "") }
                                    .map { it.split(": ") }
                                    .filter { it.size == 2 }
                                    .associate { it[0].toLowerCase() to it[1] }

                            //requestHeaders.forEach { entry ->
                            //    println("  [${entry.key}]: '${entry.value}'")
                            //}

                            //val status = readFileData("$SERVICE_PATH/${requestHeaders["host"]}/supervise/stat")
                            //val start = now()
                            //var result = ""
                            //val ret = runCommand(requestHeaders.getOrElse("x-command", {"s6-svstat /Users/james/projects/nginx-repo/s6/nginx"})) {
                            //    result = it
                            //}
                            //val duration = now() - start;

                            /*
                            val body = mapOf("status" to "😎 OK",
                                    "finish" to getTime(),
                                    "time" to "$duration",
                                    "result" to result).toJSON()
                                    */
                            val res = handler(HttpRequest(method, path, requestHeaders, socketfd))

                            //val ret = 0
                            //val body: String = status?.stringFromUtf8() ?: "error"
                            //val statusCode = if (status == null) 404 else 200
                            //val statusMessage = if (status == null) "NOT FOUND" else "OK"

                            //println("    --- request method=$method path=$path host=${requestHeaders["host"]}")
                            val response = (defaultHeaders() + mapOf(
                                    //"X-Return-Code" to "$ret",
                                    //"X-Duration" to "$duration",
                                    "Content-Type" to "text/plain; charset=utf-8",
                                    "Content-Length" to "${res.body.cstr.size}",
                                    "X-Request-Count" to "$hit"
                            )).flatMap { listOf("${it.key}: ${it.value}") }
                                    .joinToString("\n",
                                            "HTTP/1.1 ${res.statusCode} ${res.statusMessage}\n", "\n\n${res.body}")
                            //val response = "HTTP/1.1 200 OK\nServer: kweb/0.0\nContent-Length: 0\nConnection: close\nContent-Type: text/plain\nX-Request-Count: $hit\n\n"

                            write(socketfd, response.cstr, response.cstr.size.toLong())
                                    .ensureUnixCallResult { it >= 0 }
                        }
                    }
                    close(socketfd)
                    if (hit % 500 == 0) {
                        println("Access #: $hit")
                    }
                    if (shouldFork) exit(1)
                    //web(socketfd, hit++)
                    //return
                } else {
                    close(socketfd)
                }
            }
        }
    }
}

fun defaultHeaders() = mapOf(
        "Server" to "kweb/0.0",
        "Connection" to "close")

// Not available through interop because declared as macro:
fun htons(value: Short) = ((value.toInt() ushr 8) or (value.toInt() shl 8)).toShort()

fun throwUnixError(): Nothing {
    perror(null) // TODO: store error message to exception instead.
    throw Error("UNIX call failed")
}

inline fun Int.ensureUnixCallResult(predicate: (Int) -> Boolean): Int {
    if (!predicate(this)) {
        throwUnixError()
    }
    return this
}

inline fun Long.ensureUnixCallResult(predicate: (Long) -> Boolean): Long {
    if (!predicate(this)) {
        throwUnixError()
    }
    return this
}

fun now(): Long = time(null)

fun getTime(): String {
    val now: time_t = time(null)
    //val mytime: CPointer<tm>? = localtime(cValuesOf(now))

    val time: String = ctime(cValuesOf(now))?.toKString()?.removeSuffix("\n") ?: "<null>"

    return time;
}

fun Map<String, String>.toJSON(): String = entries.map { "\"${it.key}\": \"${it.value}\"" }
            .joinToString(",", "{", "}")

val String.firstLine: String
    get() {
        val nl = indexOf('\n').let { if(it > 0) it else length }
        val cr = indexOf('\r').let { if(it > 0) it else length }
        val lastIndex = min(nl, cr)
        //println("lastIndex: $lastIndex length: $length")
        return StringBuilder().append(subSequence(0, lastIndex)).toString()
    }
