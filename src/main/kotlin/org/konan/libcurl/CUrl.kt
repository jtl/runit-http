package org.konan.libcurl

import kotlinx.cinterop.*
import platform.posix.*
import platform.posix.size_t
import libcurl.*

class CUrl(val url: String) {
    val stableRef = StableRef.create(this)

    val curl = curl_easy_init()

    init {
        val header = staticCFunction(::header_callback)
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, header)
        curl_easy_setopt(curl, CURLOPT_HEADERDATA, stableRef.asCPointer())
        val write_data = staticCFunction(::write_callback)
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data)
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, stableRef.asCPointer())
        url(url)
    }

    val header = Event<String>()
    val body = Event<String>()
    fun url(url: String) = curl_easy_setopt(curl, CURLOPT_URL, url)
    fun verbose() = curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L)
    fun header() = curl_easy_setopt(curl, CURLOPT_HEADER, 1L)
    fun showprogress() = curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L)
    fun nosignal() = curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1L)
    fun wilcardmatch() = curl_easy_setopt(curl, CURLOPT_WILDCARDMATCH, 1L)
    fun timeoutMs(t: Long) = curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, t);
    fun timeout(t: Long) = curl_easy_setopt(curl, CURLOPT_TIMEOUT, t);
    //fun wilcardmatch() = curl_easy_setopt(curl, CURLOPT_WILDCARDMATCH, 1L)

    fun reset() = curl_easy_reset(curl)
    fun postFields(data: String) = curl_easy_setopt(curl, CURLOPT_COPYPOSTFIELDS, data)
    fun unixSocket(path: String) = curl_easy_setopt(curl, CURLOPT_UNIX_SOCKET_PATH, path)
    fun post() = curl_easy_setopt(curl, CURLOPT_POST, 1L)
    fun postSize(size: Long) = curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, size)
    fun httpPost() = curl_easy_setopt(curl, CURLOPT_HTTPPOST, 1L)
    fun nobody() = curl_easy_setopt(curl, CURLOPT_NOBODY, 1L)

    fun fetch() {
        val res = curl_easy_perform(curl)
        if (res != CURLE_OK)
            throw CurlException(res)
    }

    fun close() {
        curl_easy_cleanup(curl)
        stableRef.dispose()
    }

    class CurlException(res: Int) :
            Exception("curl_easy_perform() failed: ${curl_easy_strerror(res)?.toKString()}")

}

fun CPointer<ByteVar>.toKString(length: Int): String {
    val bytes = this.readBytes(length)
    return bytes.stringFromUtf8()
}

fun header_callback(buffer: CPointer<ByteVar>?, size: size_t, nitems: size_t, userdata: COpaquePointer?): size_t {
    if (buffer == null) return 0
    if (userdata != null) {
        val header = buffer.toKString((size * nitems).toInt()).trim()
        val curl = userdata.asStableRef<CUrl>().get()
        curl.header(header)
    }
    return size * nitems
}


fun write_callback(buffer: CPointer<ByteVar>?, size: size_t, nitems: size_t, userdata: COpaquePointer?): size_t {
    if (buffer == null) return 0
    if (userdata != null) {
        val data = buffer.toKString((size * nitems).toInt()).trim()
        val curl = userdata.asStableRef<CUrl>().get()
        curl.body(data)
    }
    return size * nitems
}
