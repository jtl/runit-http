package kommon

import platform.posix.*
import kotlinx.cinterop.*

fun allocMemory(size: Int) = calloc(1, size.signExtend())
fun freeMemory(ptr: COpaquePointer) = free(ptr)

fun random() = platform.posix.random()

fun readFileData(path: String): ByteArray? {
    return memScoped {
        val info = alloc<stat>()
        if (stat(path, info.ptr) != 0) return null
        // We're not planning to serve files > 4G, right?
        val size = info.st_size.toInt()
        val result = ByteArray(size)
        val file = fopen(path, "rb")
        if (file == null) return null
        var position = 0
        while (position < size) {
            val toRead = minOf(size - position, 4096)
            val read = fread(result.refTo(position), 1, toRead.signExtend(), file).toInt()
            if (read <= 0) break
            position += read
        }
        fclose(file)
        result
    }
}

fun runCommand(path: String, fn: (String) -> Unit): Int {
    return memScoped {
        val builder = StringBuilder()
        val result = ByteArray(4096)
        val file = popen(path, "r")
        if (file == null) return -1
        var position = 0
        while (fgets(result.refTo(0), result.size, file) != null) {
            val iterator = result.iterator()
            var byte: Byte = iterator.nextByte()
            while(byte > 0) {
                builder.append(byte.toChar())
                byte = iterator.nextByte()
            }
        }
        fn(builder.toString())
        pclose(file)
    }
}

fun writeToFileData(path: String, data: ByteArray, append: Boolean = false) {
    return memScoped {
        val file = fopen(path, if (append) "ab" else "wb")
        if (file == null) throw Error("Cannot write to $file")
        var position = 0
        while (position < data.size) {
            val toWrite = minOf(data.size - position, 4096)
            val written = fwrite(data.refTo(position), 1, toWrite.signExtend(), file).toInt()
            if (written <= 0) break
            position += written
        }
        fclose(file)
    }
}